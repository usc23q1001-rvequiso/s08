from django.urls import path
from . import views

# Syntax
# path (route, view, name)

urlpatterns = [
	path('', views.index, name='index'),
	path('eh', views.yeet, name='yeet'),
	path('<int:todoitem_id>/', views.todoitem, name='viewtododitem')
]
